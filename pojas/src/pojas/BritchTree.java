package pojas;

public class BritchTree implements Tree{
	public int ID;
	public BritchTree (int ID){
		 this.ID = ID ;
	}
		
	@Override
	public void grow() {
		System.out.println("I'm a growing britch tree.");
	}

	@Override
	public void looseLeaves() {
		System.out.println("I lost my all leaves because I'm a britch tree.");
		
	}
}