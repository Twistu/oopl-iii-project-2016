package pojas;

public interface Plant {
	public void grow();
	public void looseLeaves();
}

