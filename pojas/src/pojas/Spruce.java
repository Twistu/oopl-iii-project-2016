package pojas;

public class Spruce implements Tree{
	public int ID;
	public	Spruce (int ID){
		 this.ID = ID ;
	}
		
	@Override
	public void grow() {
		System.out.println("I'm a growing spruce.");
	}

	@Override
	public void looseLeaves() {
		System.out.println("I'm spruce and I don't loose my leaves.");
			
	}
}
