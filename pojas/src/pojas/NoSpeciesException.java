package pojas;


public class NoSpeciesException extends Exception {
	
	public NoSpeciesException(){
		super("No such a species exists in my database");
	}
	
}
