package pojas;

public class Pine implements Tree {
	public int ID;
	public Pine (int ID){
		 this.ID = ID ;
	}
	
	@Override
	public void grow() {
		System.out.println("I'm a growing pine.");
	}

	@Override
	public void looseLeaves() {
		System.out.println("I'm pine and I don't loose my leaves.");
		
	}
	
}
