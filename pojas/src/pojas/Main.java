package pojas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) throws NoSpeciesException {
		Tree a = new Oak(1);
		Plant b = new Oak(2);
		Plant c = new Pine(3);
		Plant d = new Pine(4);
		Plant e = new Spruce(5);
		Plant f = new Spruce(6);
		Plant g = new BritchTree(7);
		List<Plant> list = new ArrayList<Plant>();
		list.add(a);
		list.add(b);
		list.add(c);
		list.add(d);
		list.add(e);
		list.add(f);
		list.add(g);
		
		Map<String, String> Fruits = new HashMap<String, String>();
		Fruits.put("Oak","acorn");
		Fruits.put("Pine","cone");
		Fruits.put("Spruce","cone");
		Fruits.put("BritchTree","peanuts birch");
			
		
		for(Plant p : list){
			if(Fruits.get(p.getClass().getSimpleName())==null){
				throw new NoSpeciesException();
			}	
			p.grow();
			p.looseLeaves();
			System.out.println("I give birth to "+Fruits.get(p.getClass().getSimpleName()));
			System.out.println();
			
		}
		
	}

}
