package pojas;

public class Oak implements Tree {
	public int ID;
	public Oak (int ID){
		 this.ID = ID ;
	}
	
	@Override
	public void grow() {
		System.out.println("I'm a growing oak.");
	}

	@Override
	public void looseLeaves() {
		System.out.println("I lost my all leaves because I'm an oak.");
		
	}
	
}